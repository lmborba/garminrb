require "garminrb/version"
require 'oauth'

module GarminRB

  class AccessToken

    attr_accessor :access_token

    def initialize(token)
      self.access_token = token
    end

    def hash
      {
        access_token: self.access_token.token,
        access_secret: self.access_token.secret
      }
    end

    def token
      self.access_token.token
    end

    def secret
      self.access_token.secret
    end

  end

  class Authorization

    attr_accessor :request_token
    attr_accessor :client

    def initialize(token,client)
      self.request_token = token
      self.client = client
    end

    def url
      self.request_token.authorize_url(oauth_callback: self.client.callback_url).sub! 'https://connectapi.', 'https://connect.'
    end

    def hashify
      {
        oauth_token: self.request_token.token,
        oauth_token_secret: self.request_token.secret
      }
    end

    def verify(token_verification)
      token = self.request_token.get_access_token(oauth_verifier: token_verification)
      AccessToken.new(token)
    end

  end

  class Client

    attr_accessor :consumer
    attr_accessor :callback_url

    def initialize(consumer_key,consumer_secret,callback_url)

      self.consumer = OAuth::Consumer.new(
        consumer_key,consumer_secret,
        :site => "https://connectapi.garmin.com",
        :request_token_path => "/oauth-service/oauth/request_token",
        :access_token_path  => "/oauth-service/oauth/access_token",
        :authorize_path     => "/oauthConfirm"
      )
      self.callback_url = callback_url

    end

    def start_from_hash(hash)
      request_token = OAuth::RequestToken.from_hash(self.consumer,hash)
      Authorization.new(request_token,self)
    end

    def start_authorization
      request_token = self.consumer.get_request_token(:oauth_callback => self.callback_url)
      Authorization.new(request_token,self)
    end

  end

end
